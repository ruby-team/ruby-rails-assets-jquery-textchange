# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rails-assets-jquery-textchange/version'

Gem::Specification.new do |spec|
  spec.name          = "rails-assets-jquery-textchange"
  spec.version       = RailsAssetsJqueryTextchange::VERSION
  spec.authors       = ["rails-assets.org"]
  spec.description   = "jQuery TextChange Plugin"
  spec.summary       = "jQuery TextChange Plugin"
  spec.homepage      = "http://zurb.com/playground/jquery-text-change-custom-event"
  spec.license       = "MIT"

  spec.files         = `find ./* -type f | cut -b 3-`.split($/)
  spec.require_paths = ["lib"]

  spec.add_dependency "rails-assets-jquery", ">= 0"
  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
